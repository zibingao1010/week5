# Week 5 Mini-Project Requirements
Create a Rust AWS Lambda function (or app runner)

Implement a simple service

Connect to a database

## Steps
1. Initialize your Cargo Lambda project using the command `cargo lambda new <YOUR-PROJECT-NAME>`. 
2. Modify the `Cargo.toml` and `src/main.rs` file according to your design and requirements. I implemented a Lambda function that will return the name of a person given the specific age and gender.
3. Navigate to the AWS IAM Management Console to create a new IAM User for credential management. Attach policies `AWSLambda_FullAccess` and `IAMFullAccess`.
4. Under `Security Sredentials` section, generate an access key for API access. 
5. run `aws configure` in the terminal and enter the generated key as prompted.
6. Build the project by running `cargo lambda build --release`.
7. Deploy the project by running `cargo lambda deploy`.
8. Go to AWS console and enter AWS Lambda, navigate into your lambda function, then under `Configuration`, click `Permission`, then click the link under your `Role name`. 
9. In the new tab, attach policies `AmazonDynamoDBFullAccess` and `AWSLambdaBasicExecutionRole` to your role.
10. Navigate to the AWS console to establish a DynamoDB table. Click on the `Items` tab, then click on `Create item`.
11. Choose a name for your table. The name should align with that in your `main.rs` file.
12. Enter the details for your item
13. Initiate a new API setup, opting for the default REST API option, and then click on the initialized API and create a new resource for your API's endpoint.
14. For the created resource, implement an `ANY` method and associate it with your Lambda function.
15. Deploy the API by clicking the button on the top right. Create a new stage called `Test` for instance.
15. Go to `stages` and find your invoke URL.
16. You can test your API gateway using `curl` request.

## Screenshots
* AWS Lambda
![image](Lambda.png)

* DynamoDB
![image](Items.png)

* Tests
![image](Tests.png)




